import React, { useState } from "react";
import parse from "html-react-parser";

import "./NewsData.css";

// function to give font weight based on score
const fontWeight = (score) => {
	let weight;
	if (score < 100) {
		weight = 500;
	} else if (score < 200) {
		weight = 600;
	} else if (score < 300) {
		weight = 700;
	} else {
		weight = 800;
	}
	return weight;
};

// news Card for each story
const NewsData = ({ post }) => {
	const [story, setStory] = useState(post);

	const { title, text, by, score } = story;
	return (
		<div className="pa4 storyCard">
			<h2 style={{ fontWeight: fontWeight(score) }}>{title}</h2>
			<div className="mb2 flex justify-between">
				<span>{`Author: ${by}`}</span>
				<span>{`Score: ${score}`}</span>
			</div>
			<span className="mb3 pa1 pt2 para" style={{ wordBreak: "break-word" }}>
				{text && parse(text)}
			</span>
			<span className="center">• • • •</span>
		</div>
	);
};

export default NewsData;
