import React, { useState, useEffect } from "react";
import NewsData from "../NewsCard/NewsData";
import { getPosts } from "../hnAPI";
import "tachyons";
import Fade from "react-reveal/Fade";
import "./NewsPaper.css";

// Newspaper container to display news
const NewsPaper = () => {
	const [posts, setPosts] = useState([]);
	const [isDark, setDark] = useState(false);

	const onDarkMode = () => setDark(!isDark);

	const themeClassName = isDark ? "dark" : "light";
	const themeTitle = isDark ? "dark-title" : "light-title";

	useEffect(() => {
		getPosts().then((posts) => posts && setPosts(posts));
	}, []);

	if (posts.length === 0) {
		return (
			<div className="loader">
				<h1 className={themeTitle}>Hacker News</h1>
				<progress className="center pure-material-progress-circular" />
			</div>
		);
	}

	return (
		// <Fade cascade bottom distance="50px">
		<div className={`pl3 pr3 ${themeClassName}`}>
			<h1 className={`center pa2 ${themeTitle}`}>Hacker News</h1>
			<span className="fab pa1" onClick={onDarkMode}>
				<img
					alt="dark"
					src="https://img.icons8.com/emoji/36/000000/first-quarter-moon.png"
				/>
			</span>

			<div className="grid-layout">
				{posts.map((post, index) => (
						<Fade bottom distance="50px" key={index.toString()}>
							<NewsData post={post}  />
						</Fade>
				))}
			</div>
			{/* </Fade> */}
		</div>
		// </Fade>
	);
};

export default NewsPaper;
