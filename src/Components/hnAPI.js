export const baseURL = "https://hacker-news.firebaseio.com/v0/";
export const askStoriesURL = `${baseURL}askstories.json`;
export const storyURL = `${baseURL}item/`;

export const getStoryIds = async () => {
	const result = await fetch(askStoriesURL).then((data) => data.json());
	return result;
};

export const getStoryById = async (storyId) => {
	const storyByIdUrl = `${storyURL}/${storyId}.json`;
	const storyResult = await fetch(storyByIdUrl).then((data) => data.json());
	return storyResult;
};

export const getPosts = async () => {
	let idsResponse = await fetch(
		"https://hacker-news.firebaseio.com/v0/askstories.json"
	);

	let idsData = await idsResponse.json();

	let posts = await Promise.all(
		idsData.map(async (id) => {
			let response = await fetch(
				`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
			);

			let data = await response.json();
			return data;
		})
	);
	return posts;
};
